package com.example.numerology;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.List;


public class InputPage extends AppCompatActivity{

    public static int birthYear,birthMonth,birthDay,birthTime;
    public static String gender;
    private Spinner spinnerGender;

    EditText editTextBirthYear, editTextBirthMonth, editTextBirthDay, editTextBirthTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_page);

        editTextBirthYear = findViewById(R.id.birth_year);
        editTextBirthMonth = findViewById(R.id.birth_month);
        editTextBirthDay = findViewById(R.id.birth_day);
        editTextBirthTime = findViewById(R.id.birth_time);

        List<String> genders = Arrays.asList("男","女","");
        spinnerGender = findViewById(R.id.gender);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,genders);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter);
        spinnerGender.setSelection(genders.size()-1);
    }

    public void enter2(View v){

        if (!HelperFunction.allEnteredValid(editTextBirthYear,editTextBirthMonth,editTextBirthDay,editTextBirthTime,spinnerGender)){
            Toast.makeText(this, "请正确输入", Toast.LENGTH_SHORT).show();
            return;
        }
        birthYear = Integer.parseInt(editTextBirthYear.getText().toString());
        birthMonth = Integer.parseInt(editTextBirthMonth.getText().toString());
        birthDay = Integer.parseInt(editTextBirthDay.getText().toString());
        birthTime = Integer.parseInt(editTextBirthTime.getText().toString());
        gender = spinnerGender.getSelectedItem().toString();

        startActivity(new Intent(InputPage.this,ResultPage1.class));
    }

}
