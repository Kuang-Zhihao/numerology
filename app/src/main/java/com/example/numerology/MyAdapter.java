package com.example.numerology;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private String dayunName[];
    private String age[];
    private Context context;

    public MyAdapter(Context context, String dayunName[],String age[]){
        this.context = context;
        this.dayunName = dayunName;
        this.age = age;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dayun_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.textDayunName.setText(dayunName[position]);
        holder.textAge.setText(age[position]);
    }

    @Override
    public int getItemCount() {
        return dayunName.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textDayunName;
        TextView textAge;
        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            textDayunName = itemView.findViewById(R.id.dayun_name);
            textAge = itemView.findViewById(R.id.age);
        }
    }
}
