package com.example.numerology;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.InputStream;
import java.util.List;

public class ResultPage1 extends AppCompatActivity {

    public static int yearTianganIndex;
    public static int yearDizhiIndex;
    public static int monthTianganIndex;
    public static int monthDizhiIndex;
    public static int dayTianganIndex;
    public static int dayDizhiIndex;
    public static int timeTianganIndex;
    public static int timeDizhiIndex;
    public static int yearRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_page_1);

        int birthYear = InputPage.birthYear;
        int birthMonth = InputPage.birthMonth;
        int birthDay = InputPage.birthDay;
        int birthTime = InputPage.birthTime;

        InputStream is = getResources().openRawResource(R.raw.monthgaptable);
        List<List<Integer>> monthGapTable = HelperFunction.readTable(is);
        is = getResources().openRawResource(R.raw.monthtable);
        List<List<Integer>> monthTable = HelperFunction.readTable(is);
        is = getResources().openRawResource(R.raw.timetable);
        List<List<Integer>> timeTable = HelperFunction.readTable(is);

        String[] tiangan = {"Header","甲","乙","丙","丁","戊","己","庚","辛","壬","癸"};
        String[] dizhi = {"Header","子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"};
        int[] monthDay = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        yearRow = birthYear - monthGapTable.get(1).get(0) + 1;
        switch (birthMonth) {
            case 1:
                yearTianganIndex = (birthYear - 1 + 6) % 10 + 1;
                yearDizhiIndex = (birthYear - 1 + 8) % 12 + 1;
                if (birthDay < monthGapTable.get(yearRow).get(birthMonth)) {
                    monthDizhiIndex = 1;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(1);
                } else if (birthDay >= monthGapTable.get(yearRow).get(birthMonth)) {
                    monthDizhiIndex = 2;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(2);
                }
                break;
            case 2:
                if (birthDay < monthGapTable.get(yearRow).get(birthMonth)) {
                    yearTianganIndex = (birthYear - 1 + 6) % 10 + 1;
                    yearDizhiIndex = (birthYear - 1 + 8) % 12 + 1;
                    monthDizhiIndex = 2;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(2);
                } else if (birthDay >= monthGapTable.get(yearRow).get(birthMonth)) {
                    yearTianganIndex = (birthYear + 6) % 10 + 1;
                    yearDizhiIndex = (birthYear + 8) % 12 + 1;
                    monthDizhiIndex = 3;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(3);
                }
                break;
            case 12:
                yearTianganIndex = (birthYear + 6) % 10 + 1;
                yearDizhiIndex = (birthYear + 8) % 12 + 1;
                if (birthDay < monthGapTable.get(yearRow).get(birthMonth)) {
                    monthDizhiIndex = 12;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(12);
                } else if (birthDay >= monthGapTable.get(yearRow).get(birthMonth)) {
                    monthDizhiIndex = 1;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(1);
                }
                break;
            default:
                yearTianganIndex = (birthYear + 6) % 10 + 1;
                yearDizhiIndex = (birthYear + 8) % 12 + 1;
                if (birthDay < monthGapTable.get(yearRow).get(birthMonth)) {
                    monthDizhiIndex = birthMonth;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(birthMonth);
                } else if (birthDay >= monthGapTable.get(yearRow).get(birthMonth)) {
                    monthDizhiIndex = birthMonth + 1;
                    monthTianganIndex = monthTable.get(yearTianganIndex).get(birthMonth + 1);
                }
        }
        if (birthYear % 4 == 0) monthDay[1] = 29;
        monthDay[birthMonth - 1] = birthDay;
        int N = birthYear % 100;
        int E;
        if (birthYear < 2000) E = 15; else E = 0;
        int D = 0;
        for (int I=0; I<birthMonth; I++) D = D + monthDay[I];
        if (birthTime == 23) D = D + 1; else if (birthTime == 0) D = D - 1;
        int A = (N-1) / 4;
        if (N-1 < 0) A = -1;
        int B = ((N-1) % 12) * 5 + E;
        int C = D % 60;
        dayTianganIndex = HelperFunction.newMod((A+B+C) % 60, 10);
        dayDizhiIndex = HelperFunction.newMod((A+B+C) % 60, 12);
        timeDizhiIndex = HelperFunction.newMod((int) Math.ceil((birthTime+2)/2.0), 12);
        timeTianganIndex = timeTable.get(dayTianganIndex).get(timeDizhiIndex);

        TextView nianzhu = findViewById(R.id.nianzhu);
        TextView yuezhu = findViewById(R.id.yuezhu);
        TextView rizhu = findViewById(R.id.rizhu);
        TextView shizhu = findViewById(R.id.shizhu);
        TextView nianzhuShuxing = findViewById(R.id.nianzhu_shuxing);
        TextView yuezhuShuxing = findViewById(R.id.yuezhu_shuxing);
        TextView rizhuShuxing = findViewById(R.id.rizhu_shuxing);
        TextView shizhuShuxing = findViewById(R.id.shizhu_shuxing);
        nianzhu.setText(tiangan[yearTianganIndex] + dizhi[yearDizhiIndex]);
        yuezhu.setText(tiangan[monthTianganIndex] + dizhi[monthDizhiIndex]);
        rizhu.setText(tiangan[dayTianganIndex] + dizhi[dayDizhiIndex]);
        shizhu.setText(tiangan[timeTianganIndex] + dizhi[timeDizhiIndex]);
        nianzhuShuxing.setText(HelperFunction.getShuxing(tiangan[yearTianganIndex]) + HelperFunction.getShuxing(dizhi[yearDizhiIndex]));
        yuezhuShuxing.setText(HelperFunction.getShuxing(tiangan[monthTianganIndex]) + HelperFunction.getShuxing(dizhi[monthDizhiIndex]));
        rizhuShuxing.setText(HelperFunction.getShuxing(tiangan[dayTianganIndex]) + HelperFunction.getShuxing(dizhi[dayDizhiIndex]));
        shizhuShuxing.setText(HelperFunction.getShuxing(tiangan[timeTianganIndex]) + HelperFunction.getShuxing(dizhi[timeDizhiIndex]));
    }

    public void enter3(View v) {
        Intent goToSecond = new Intent();
        goToSecond.setClass(this, ResultPage2.class);
        startActivity(goToSecond);
    }
}
