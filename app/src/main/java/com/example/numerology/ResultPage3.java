package com.example.numerology;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ResultPage3 extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_page_3);
        int birthYear = InputPage.birthYear;
        String[][] dayun = ResultPage2.dayun;
        List<Integer> output1 = new ArrayList<>();
        List<String> output2 = new ArrayList<>();
        String[] yunshi = {"技艺","官运","财运","心智","人际"};
        String yunName, suiName;
        int yunYear, suiYear;
        int tenMultiple = ResultPage2.tenMultiple;
        for (int I=0; I<tenMultiple; I++){
            yunName = dayun[1][I];
            yunYear = Integer.parseInt(dayun[3][I]);
            for (int J=0; J<9; J++){
                suiYear = yunYear + J;
                suiName = HelperFunction.getYearName(suiYear);
                if (yunName.equals(suiName)){
                    output1.add(suiYear - birthYear);
                    output2.add(yunshi[HelperFunction.getShishenIndex(dayun[0][I])-1]);
                }
            }
        }
        String message = "";
        for (int K=0; K<output1.size(); K++)
            message = message + output1.get(K) + "岁要注意，" + output2.get(K) + "方面力量旺盛。";
        TextView messageTextview = findViewById(R.id.message);
        messageTextview.setText(message);
    }
}
