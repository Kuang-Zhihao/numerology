package com.example.numerology;

import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


public class HelperFunction{

    public static List<List<Integer>> readTable(InputStream is) {
        List<List<Integer>> output = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        try {
            String line;
            while ((line = reader.readLine()) != null){
                String[] tokens = line.split(",");
                List<Integer> row = new ArrayList<>();
                for (String token:tokens) row.add(Integer.parseInt(token));
                output.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static int newMod(int dividend, int divisor){
        if (dividend < 0) dividend = dividend + divisor;
        int output = dividend % divisor;
        if (output == 0) output = divisor;
        return output;
    }

    public static String getShuxing(String tianganDizhi){
        switch (tianganDizhi){
            case "甲":
            case "乙":
            case "寅":
            case "卯":
                return "木";
            case "丙":
            case "丁":
            case "巳":
            case "午":
                return "火";
            case "戊":
            case "己":
            case "丑":
            case "辰":
            case "未":
            case "戌":
                return "土";
            case "庚":
            case "辛":
            case "申":
            case "酉":
                return "金";
            case "壬":
            case "癸":
            case "子":
            case "亥":
                return "水";
        }
        return "";
    }

    public static String getYearName(int year) {
        String[] tiangan = {"Header","甲","乙","丙","丁","戊","己","庚","辛","壬","癸"};
        String[] dizhi = {"Header","子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"};
        int year_tiangan_index = (year + 6) % 10 + 1;
        int year_dizhi_index = (year + 8) % 12 + 1;
        return tiangan[year_tiangan_index] + dizhi[year_dizhi_index];
    }

    public static int getShishenIndex(String shishen){
        switch (shishen){
            case "食神":
            case "伤官":
                return 1;
            case "正官":
            case "七杀":
                return 2;
            case "偏财":
            case "正财":
                return 3;
            case "正印":
            case "偏印":
                return 4;
            case "比肩":
            case "劫财":
                return 5;
            default:
                return 0;
        }
    }

    public static boolean allEnteredValid(EditText editTextBirthYear,EditText editTextBirthMonth,EditText editTextBirthDay,EditText editTextBirthTime, Spinner spinnerGender){

        String stringBirthYear = editTextBirthYear.getText().toString();
        String stringBirthMonth = editTextBirthMonth.getText().toString();
        String stringBirthDay = editTextBirthDay.getText().toString();
        String stringBirthTime = editTextBirthTime.getText().toString();
        String gender = spinnerGender.getSelectedItem().toString();

        // Note we have limited to inputType to number, but still can get ""
        if (stringBirthYear.equals("") || stringBirthMonth.equals("") || stringBirthDay.equals("") || stringBirthTime.equals("") || gender.equals("")){
            return false;
        }

        int birthYear = Integer.parseInt(stringBirthYear);
        int birthMonth = Integer.parseInt(stringBirthMonth);
        int birthDay = Integer.parseInt(stringBirthDay);
        int birthTime = Integer.parseInt(stringBirthTime);

        if (!(birthYear <= 2021 && birthYear >= 1961) || !(birthMonth <= 12 && birthMonth >= 1) || !(birthDay <= 31 && birthDay >= 1) || !(birthTime >= 0 && birthTime <= 23)){
            return false;
        }

        return true;

    }
}
