package com.example.numerology;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.io.InputStream;
import java.util.List;

public class ResultPage2 extends AppCompatActivity {

    public static int tenMultiple = 10;
    public static String[][] dayun = new String[4][tenMultiple];

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_page_2);
        int yearTianganIndex = ResultPage1.yearTianganIndex;
        int monthTianganIndex = ResultPage1.monthTianganIndex;
        int monthDizhiIndex = ResultPage1.monthDizhiIndex;
        int dayTianganIndex = ResultPage1.dayTianganIndex;
        int yearRow = ResultPage1.yearRow;
        int birthYear = InputPage.birthYear;
        int birthMonth = InputPage.birthMonth;
        int birthDay = InputPage.birthDay;
        String gender = InputPage.gender;
        int dayunTianganIndex = monthTianganIndex;
        int dayunDizhiIndex = monthDizhiIndex;

        InputStream is = getResources().openRawResource(R.raw.monthgaptable);
        List<List<Integer>> monthGapTable = HelperFunction.readTable(is);
        String[] tiangan = {"header","甲","乙","丙","丁","戊","己","庚","辛","壬","癸"};
        String[] dizhi = {"header","子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"};
        int[] monthDay = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String [][] shishenTable = {
                {"日元天干","甲","乙","丙","丁","戊","己","庚","辛","壬","癸"},
                {"甲","比肩","劫财","食神","伤官","偏财","正财","七杀","正官","偏印","正印"},
                {"乙","劫财","比肩","伤官","食神","正财","偏财","正官","七杀","正印","偏印"},
                {"丙","偏印","正印","比肩","劫财","食神","伤官","偏财","正财","七杀","正官"},
                {"丁","正印","偏印","劫财","比肩","伤官","食神","正财","偏财","正官","七杀"},
                {"戊","七杀","正官","偏印","正印","比肩","劫财","食神","伤官","偏财","正财"},
                {"己","正官","七杀","正印","偏印","劫财","比肩","伤官","食神","正财","偏财"},
                {"庚","偏财","正财","七杀","正官","偏印","正印","比肩","劫财","食神","伤官"},
                {"辛","正财","偏财","正官","七杀","正印","偏印","劫财","比肩","伤官","食神"},
                {"壬","食神","伤官","偏财","正财","七杀","正官","偏印","正印","比肩","劫财"},
                {"癸","伤官","食神","正财","偏财","正官","七杀","正印","偏印","劫财","比肩"}
        };

        boolean forward = (gender.equals("男") && yearTianganIndex % 2 == 1) || (gender.equals("女") && yearTianganIndex % 2 == 0);
        boolean backward = (gender.equals("男") && yearTianganIndex % 2 == 0) || (gender.equals("女") && yearTianganIndex % 2 == 1);
        for (int I = 0; I<tenMultiple; I++){
            if (forward){
                dayunTianganIndex = HelperFunction.newMod(dayunTianganIndex + 1, 10);
                dayunDizhiIndex = HelperFunction.newMod(dayunDizhiIndex + 1, 12);
            } else if (backward){
                dayunTianganIndex = HelperFunction.newMod(dayunTianganIndex - 1, 10);
                dayunDizhiIndex = HelperFunction.newMod(dayunDizhiIndex - 1, 12);
            }
            dayun[0][I] = shishenTable[dayTianganIndex][dayunTianganIndex];
            dayun[1][I] = tiangan[dayunTianganIndex] + dizhi[dayunDizhiIndex];
        }
        int qiyunIndex = 0;
        if (forward) {
            if (birthMonth == 12) {
                if (birthDay <= monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = monthGapTable.get(yearRow).get(birthMonth) - birthDay;
                else if (birthDay > monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = 31 - birthDay + monthGapTable.get(yearRow + 1).get(1);
            } else {
                if (birthDay <= monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = monthGapTable.get(yearRow).get(birthMonth) - birthDay;
                else if (birthDay > monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = monthDay[birthMonth - 1] - birthDay + monthGapTable.get(yearRow).get(birthMonth + 1);
            }
        } else if (backward) {
            if (birthMonth == 1) {
                if (birthDay <= monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = 31 - monthGapTable.get(yearRow - 1).get(12) + birthDay;
                else if (birthDay > monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = birthDay - monthGapTable.get(yearRow).get(birthMonth);
            } else {
                if (birthDay <= monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = monthDay[birthMonth - 2] + birthDay - monthGapTable.get(yearRow).get(birthMonth - 1);
                else if (birthDay > monthGapTable.get(yearRow).get(birthMonth))
                    qiyunIndex = birthDay - monthGapTable.get(yearRow).get(birthMonth);
            }
        }
        int qiyunAge = qiyunIndex / 3;
        int qiyunMonth = 4 * (qiyunIndex % 3);
        if (birthMonth + qiyunMonth > 12)
            qiyunAge = qiyunAge + 1;
        int dayunYear = qiyunAge + birthYear;
        for (int I=0; I<tenMultiple; I++){
            dayun[2][I] = (dayunYear - birthYear) + "岁" + qiyunMonth + "个月";
            dayun[3][I] = String.valueOf(dayunYear);
            dayunYear = dayunYear + 10;
        }

        MyAdapter myAdapter = new MyAdapter(this,dayun[0],dayun[2]);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void enter4(View v){
        Intent goToSecond = new Intent();
        goToSecond.setClass(this,ResultPage3.class);
        startActivity(goToSecond);
    }
}
